import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public panier: string;
  public paniers: string[] = [];

  constructor() {

  }

  deletePanier(panier: string) {
    this.paniers.splice(this.paniers.indexOf(panier), 1);
  }
}
